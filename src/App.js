import React, { Component } from 'react';
import logo from './logo.svg';
import './App.scss';
// import Table from './Table.js';
import Pusher from 'pusher-js';
import { AgGridReact } from 'ag-grid-react';
import { AllCommunityModules } from '@ag-grid-community/all-modules';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';
import 'ag-grid-enterprise';
import 'local-storage'


class App extends Component {


  constructor(props) {
    super(props);
    this.state = {
      modules: AllCommunityModules,
      columnDefs: [
        { headerName: "Transazione", field: "id_transazione"},
        { headerName: "Azienda", field: "id_azienda", checkboxSelection: true},
        { headerName: "Stabilimento", field: "id_stabilimento"},
        { headerName: "Terminale", field: "id_terminale"},
        { headerName: "Registrazione", field: "data_ora_registrazione"},
        { headerName: "Tessera", field: "badge"},
        { headerName: "Cognome", field: "cognome"},
        { headerName: "Nome", field: "nome"},
        { headerName: "Codice Prodotto", field: "codice_prodotto"},
        { headerName: "Desc. Prodotto", field: "desc_prodotto"},
        { headerName: "Profilo", field: "profilo"},
        { headerName: "Quantità", field: "quantita"},
        { headerName: "Prelevato", field: "prelevato", enableValue: true, allowedAggFuncs: ['sum', 'min', 'max']},
        { headerName: "Piatto", field: "nr_piatto"},
        { headerName: "Settore", field: "nr_settore"},
        { headerName: "Cella", field: "nr_cella"}
      ],
      defaultColDef: {
        sortable: true,
        filter: true,
        resizable: true,
        animateRows: true,
        flex: 1,
        enableRowGroup : true,
        enablePivot: true,
        filterParams: { newRowsAction: 'keep'},
      },
      autoGroupColumnDef: {
        headerName: 'Azienda',
        field: 'id_azienda',
        cellRenderer: 'agGroupCellRenderer',
        cellRendererParams: { checkbox: true, footerValueGetter: '"Total (" + x + ")"' },
      },

      gridOptions: {

        rowSelection: 'multiple'
      },

      rows: [],


    }


    this.insert = this.insert.bind(this);
    this.update = this.update.bind(this);
    this.delete = this.delete.bind(this);
  }

  componentDidMount() {

    //this.hydrateStateWithLocalStorage();

    this.setState({ isLoading: true });
    this.pusher = new Pusher('c117ce8aacb2b055ee8a', {
	  cluster: 'eu',
      encrypted: true,
    });


    this.channel = this.pusher.subscribe(this.getChan());
	this.channel.bind('insert', this.insert);
    this.channel.bind('update', this.update);
    this.channel.bind('delete', this.delete);



  }

  insert(data) {
    this.setState(prevState => ({
      rows: [ data, ...prevState.rows ],
    }));
  }

  update(data) {
    this.setState(prevState => ({
      rows: prevState.rows.map(el =>
              el.id === data.id ? data : el
      )
    }));
  }

  delete(id) {
    this.setState(prevState => ({
      rows: prevState.rows.filter(el => el.id !== String(id))
    }));
  }
  getYear() {
    return new Date().getFullYear().toString();
  }

  getMonth() {
    // get the month in two digit
    return ("0" + (new Date().getMonth() + 1)).slice(-2)
  }

  getChan(){

    return  "trans_" + this.getYear() + "_" + this.getMonth();
  }

  render() {
    return (
        <div  className="ag-theme-balham" style={ {height: '980px', width: '100%'} }>

        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">ArgoPro Logger</h1>
        </header>
          <button onClick={this.onButtonClick}>Get selected rows</button>
          <AgGridReact
            onGridReady={ params => this.gridApi = params.api }
            rowData={this.state.rows}
            columnDefs={this.state.columnDefs}
            groupSelectsChildren={true}
            autoGroupColumnDef={this.state.autoGroupColumnDef}
            rememberGroupStateWhenNewData={true}
            defaultColDef={this.state.defaultColDef}
            gridOptions={this.state.gridOptions}
            sideBar={true}
            animateRows={true}
             >

        </AgGridReact>

      </div>
    );
  }



  onButtonClick = () => {
    const selectedNodes = this.gridApi.getSelectedNodes()
    const selectedData = selectedNodes.map( node => node.data )
    const selectedDataString = selectedData.map( node => `${node.id_azienda}  ${node.id_stabilimento}`).join(', ')
    alert(`Selected nodes: ${selectedDataString}`)
  }

}

export default App;
