import React, { Component } from 'react';
import './App.scss';

export default class Table extends Component {
    render() {
     const rowsMapped =this.props.rows.map(row => (
            <tr key={row.id_transazione}>
              <td>{row.id_transazione}</td>
              <td>{row.id_azienda}</td>
              <td>{row.id_stabilimento}</td>
              <td>{row.id_terminale}</td>
              <td>{row.data_ora_registrazione}</td>
              <td>{row.badge}</td>
              <td>{row.cognome}</td>
              <td>{row.nome}</td>
              <td>{row.codice_prodotto}</td>
               <td>{row.desc_prodotto}</td>
              <td>{row.profilo}</td>
              <td>{row.quantita}</td>
               <td>{row.prelevato}</td>
              <td>{row.nr_piatto}</td>
              <td>{row.nr_settore}</td>
              <td>{row.nr_cella}</td>
            </tr>
          ));

      return (
        <div class="Container">
		<div class="table-responsive">
		<table class="table table-striped">
          <thead>
            <tr>
              <th>ID Transazione</th>
              <th>ID Azienda</th>
              <th>ID Stabilimento</th>
			  <th>ID Terminale</th>
              <th>Data Registrazione</th>
              <th>Badge</th>
              <th>Cognome</th>
              <th>Nome</th>
              <th>Codice prodotto</th>
              <th>Descrizione Prodotto</th>
              <th>Profilo</th>
              <th>Quantita</th>
              <th>Prelevato</th>
              <th>Nr Piatto</th>
              <th>Nr Settore</th>
              <th>Nr Cella</th>
            </tr>
          </thead>
          <tbody>
            {rowsMapped}
          </tbody>
        </table>
		</div>
		</div>
      );

	  /**
        const rowsMapped =this.props.rows.map(row => (
         <tr key={row.id}>
         <td>{row.id}</td>
         <td>{row.name}</td>
         <td>{row.price}</td>
         </tr>
         ));

         return (
         <table className="table">
          <thead>
             <tr>
                 <th>ID</th>
                 <th>Name</th>
                 <th>Price</th>
             </tr>
         </thead>
         <tbody>
         {rowsMapped}
         </tbody>
         </table>
         );
		 */
    }
}
